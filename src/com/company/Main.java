package com.company;

import javax.swing.*;
import java.net.MalformedURLException;

//FIXME NullPointerException when nimbus look and feel is set as default! works fine setting it during the program

/**
 * Main frame including the main method
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class Main {

    /**
     *
     * @param args
     * @throws MalformedURLException
     */
    public static void main(String[] args) throws MalformedURLException {
        DownloadManagerFrame.getInstance();
        Load load = new Load();

        try {
            UIManager.setLookAndFeel(Setting.getLFClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        } catch (Exception e){
            System.out.println("exception :]");
        }

        DownloadManagerFrame.getInstance().colorButtons();
//        UIManager.setLookAndFeel(new NimbusLookAndFeel());
//        SwingUtilities.updateComponentTreeUI(DownloadManagerFrame.getInstance());
    }
}

