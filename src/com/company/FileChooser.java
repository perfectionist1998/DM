package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
/**
 * the file chooser frame including JFileChooser
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class FileChooser extends JFrame{
    //the directory path
    private String path;
    //the file chooser
    private JFileChooser fileChooser;
    //this frame
    private JFrame jFrame = this;

    /**
     * constructor everything happens here
     * @param jTextFieldPath the directory path
     */
    public FileChooser(JTextField jTextFieldPath){
        super("Browse");
        fileChooser = new JFileChooser();

        BufferedImage icon = null;
        try {
            icon = ImageIO.read(new File("C:\\Users\\ASUS\\Downloads\\AP\\icon\\imageIcon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setIconImage(icon);

        this.setLayout(new BorderLayout());
        this.add(fileChooser, BorderLayout.CENTER);
        this.setSize(500,500);
        this.setResizable(false);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        this.setVisible(true);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switch (e.getActionCommand()) {
                    case "ApproveSelection" :
                        path = fileChooser.getSelectedFile().getPath();
                        jTextFieldPath.setText(path);
                        jFrame.dispose();
                        break;
                    case "CancelSelection" :
                        jFrame.dispose();
                }
            }
        });
    }

    /**
     * accesses the path
     * @return the path
     */
    public String getPath() {
        return path;
    }
}
