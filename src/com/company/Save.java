package com.company;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * saves program info in file
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class Save {

    public Save(){
        serializeTasks();
        saveSettings();
    }

    /**
     * saves information about processing downloads
     */
    private void serializeTasks(){
        try (FileOutputStream fs = new FileOutputStream("list.jdm"); ObjectOutputStream obj = new ObjectOutputStream(fs)){
            obj.writeInt(Task.tasks.size());
            for(Task t : Task.tasks)
                obj.writeObject(t);
            obj.writeInt(Task.queueTasks.size());
            for(Task t : Task.queueTasks)
                obj.writeObject(t);
            obj.writeInt(Task.completedTasks.size());
            for(Task t : Task.completedTasks)
                obj.writeObject(t);
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * saves settinf in file
     */
    private void saveSettings(){
        try (FileOutputStream fs = new FileOutputStream("setting.jdm"); ObjectOutputStream obj = new ObjectOutputStream(fs)){

            obj.writeObject(Setting.getNum());
            if(Setting.getLFClassName() == null)
                Setting.setLFClassName("");
            obj.writeObject(Setting.getLFClassName());
            if(Setting.getTheLocation() == null)
                Setting.setLocation("");
            obj.writeObject(Setting.getTheLocation());
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
