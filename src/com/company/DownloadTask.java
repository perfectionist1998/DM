package com.company;

import javax.swing.*;
import java.io.*;
import java.util.List;

/**
 * the class that actually downloads our file
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class DownloadTask extends SwingWorker<String,Integer> implements Serializable{
    private String urlString;
    private String directory;
    private FileOutputStream fileOutputStream;
    private String path;
    private Task task;
    //total downloaded bytes in each moment
    private int totalBytesNum;
    private long fileLength;
    //handles time between two process method calls
    private long previousTime;
    //handles bytes between two method calls
    private int previousByte;
    private OpenConnection c;

    /**
     * constructor initializes fields
     * @param task the task
     * @param urlString the url
     * @param directory the path of file
     */
    public DownloadTask(Task task,String urlString ,String directory){
        System.out.println("swing worker class is called");
        this.directory = directory;
        this.urlString = urlString;
        this.task = task;
    }

    /**
     * the download happens here
     * @return null
     */
    @Override
    protected synchronized String doInBackground()  {
        System.out.println("do in background");
        c = null;
        try {
            c = new OpenConnection(urlString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        path = directory + File.separator + (c != null ? c.getFileName() : null);
        InputStream inputStream = null;
        if (c != null) {
            inputStream = c.getInputStream();
        }
        try {
            fileOutputStream = new FileOutputStream(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int byteNum = 0;
        byte[] buffer = new byte[4096];
        totalBytesNum = 0;
        int percentage;
        if (c != null) {
            fileLength = c.getContentLength();
        }//intellij suggestion

        try {
            if (inputStream != null) {
                byteNum = inputStream.read(buffer);
            }//intellij suggestion
        } catch (IOException e) {
            System.out.println("opening file");
            e.printStackTrace();
        }

        previousTime = System.currentTimeMillis();
        while (!isCancelled() && byteNum != -1) {
            System.out.println("during download !");
            if ( !isDone() && (task.isPaused() == 0) ){
                System.out.println("during download !!");
                try {
                    fileOutputStream.write(buffer ,0 , byteNum);
                    totalBytesNum += byteNum;
                    percentage = (int) (totalBytesNum * 100 / fileLength);
                    if (inputStream != null) {
                        byteNum = inputStream.read(buffer);
                    }//intellij suggestion
                    setProgress(percentage);
                    publish(percentage);
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("writing buffer");
                }
            } else {
                System.out.println("during download !!!");
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.out.println("thread is sleeping");
                }
            }
            if(task.isPaused() == -1)
                break;
        }

        System.out.println("do in background is done !");

        try {
            fileOutputStream.close();
            if (c != null) {
                c.disconnect();
            }//intellij suggestion
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * done method when the download is done
     */
    @Override
    protected void done() {
        System.out.println("done in background");
        task.repaint();
        System.out.println(isCancelled());
        System.out.println(task.isPaused());
        if (!isCancelled() && task.isPaused()!= -1) {
            if(Task.tasks.contains(task)){
                Task.tasks.remove(task);
                Task.completedTasks.add(task);
                DownloadManagerFrame.getInstance().doCompleted();
            }else {
                Task.queueTasks.remove(task);
                Task.completedTasks.add(task);
                DownloadManagerFrame.getInstance().doCompleted();
            }
        }else if(task.isPaused() == -1){
            task.setLabelName("Canceled ");
        }
    }

    /**
     * the process method
     * @param values percent of download
     */
    @Override
    protected void process(List<Integer> values) {
        System.out.println("processing ...");
        long timeSpent = System.currentTimeMillis()-previousTime;
        int byteSpent = totalBytesNum- previousByte;
        int percent = values.get(values.size()-1) - values.get(0);
        System.out.println("percent" + percent);

        System.out.println("for start:");
        for (Integer i : values) {
            System.out.println(i);
            task.setProgressBarValue(i);
            task.setLabelPercentDownloaded(i);
            task.setLabelSizeDownloaded(totalBytesNum/1000);
            task.setLabelSize((double)fileLength/1000);
            task.setLabelSpeed(byteSpent/timeSpent);
            task.setLabelName(c.getFileName());
            task.setFinalPath(path);
            System.out.println("processing ...");
        }
        previousTime = System.currentTimeMillis();
        previousByte = totalBytesNum;
    }

}
