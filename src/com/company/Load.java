package com.company;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * loads program info from file
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class Load {

    //what should be in filter text area when first opens
    private static String initialTextArea;

    /**
     * constructor that loads all program info from file
     * @throws MalformedURLException
     */
    public Load() throws MalformedURLException {
        deserializeTasks();
        loadSettings();
        loadFiltered();
        System.out.println("file name :");
    }

    /**
     * loads infos about processing downloads
     */
    private void deserializeTasks(){
        int processingTasksNum;
        int queueTaskNum;
        int completedTaskNum;
        try(FileInputStream file = new FileInputStream("list.jdm"); ObjectInputStream in = new ObjectInputStream(file)) {

            processingTasksNum = in.readInt();
            for (int i = 0; i < processingTasksNum; i++) {
                Task task = (Task) in.readObject();
                task.addMouseListener(DownloadManagerFrame.getInstance().getMouseListener());
                Task.tasks.add(task);
            }
            queueTaskNum = in.readInt();
            for (int i = 0; i < queueTaskNum; i++) {
                Task task = (Task) in.readObject();
                task.addMouseListener(DownloadManagerFrame.getInstance().getMouseListener());
                Task.queueTasks.add(task);
            }
            completedTaskNum = in.readInt();
            for (int i = 0; i < completedTaskNum; i++) {
                Task task = (Task) in.readObject();
                task.addMouseListener(DownloadManagerFrame.getInstance().getMouseListener());
                Task.completedTasks.add(task);
            }
        } catch(IOException ex) {
            System.out.println("IOException is caught");
        } catch(ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException is caught");
        }
    }

    /**
     * loads settings from file
     */
    private void loadSettings(){
        try(FileInputStream file = new FileInputStream("setting.jdm"); ObjectInputStream in = new ObjectInputStream(file)) {
            Setting.setNum((Integer)in.readObject());
            Setting.setLFClassName((String)in.readObject());
            Setting.setLocation((String)in.readObject());
        } catch(IOException ex) {
            System.out.println("IOException is caught");
        } catch(ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException is caught");
        }
    }

    /**
     * loads filters from file
     * @throws MalformedURLException exception if url is not valid
     */
    private void loadFiltered() throws MalformedURLException {
        String s = "";
        try(FileReader fileReader = new FileReader("filter.jdm")){
            File tempo = new File("filter.jdm");
            char[] chars = new char[(int) tempo.length()];
            fileReader.read(chars , 0 ,(int)tempo.length() );
            s = String.valueOf(chars);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        initialTextArea = s;
        String[] filterArray = s.split("\\r?\\n");
        DownloadManagerFrame.saveFilters(filterArray);
    }

    /**
     * accesses initial text area
     * @return an string containing the initial text area
     */
    public static String getInitialTextArea() {
        return initialTextArea;
    }
}
