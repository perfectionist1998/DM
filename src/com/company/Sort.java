package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * the sort frame
 *@author Ghazale Noroozi
 * @version 0.0
 */
public class Sort extends JFrame implements ActionListener {
    //changes the order
    private JButton ok;
    //cancels
    private JButton cancel;
    private JRadioButton fileNameOrder;
    private JLabel fileNameLabel;
    private JRadioButton fileSizeOrder;
    private JLabel fileSizeLabel;
    private JRadioButton fileTimeOrder;
    private JLabel fileTimeLabel;
    private JPanel NameOrder;
    private JPanel SizeOrder;
    private JPanel TimeOrder;

    /**
     * constructor that initializes fields
     */
    public Sort(){
        this.setLayout(new GridLayout(4,0,5,5));

        JPanel buttons = new JPanel();
        cancel = new JButton("Cancel");
        ok = new JButton("OK");
        buttons.add(ok);
        buttons.add(cancel);
        fileNameLabel = new JLabel("Name");
        fileSizeLabel = new JLabel("Size");
        fileTimeLabel = new JLabel("Time");
        fileTimeOrder = new JRadioButton();
        fileSizeOrder = new JRadioButton();
        fileNameOrder = new JRadioButton();
        NameOrder = makePanelOrder(fileNameOrder , fileNameLabel ,"name");
        SizeOrder = makePanelOrder(fileSizeOrder , fileSizeLabel ,"size");
        TimeOrder = makePanelOrder(fileTimeOrder , fileTimeLabel ,"time");

        this.add(NameOrder);
        this.add(TimeOrder);
        this.add(SizeOrder);
        this.add(buttons);

        this.setSize(500,200);
        this.setResizable(true);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        BufferedImage icon = null;
        try {
            icon = ImageIO.read(new File("C:\\Users\\ASUS\\Downloads\\AP\\icon\\imageIcon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setIconImage(icon);
        this.setVisible(true);

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if(o.equals(ok)){
            this.dispose();

        }else if(o.equals(cancel)){
            this.dispose();
        }
    }

    /**
     * makes panel including a radio button and a label
     * @param jRadioButton the button
     * @param jLabel the label
     * @param status the status
     * @return the panel made
     */
    private JPanel makePanelOrder(JRadioButton jRadioButton , JLabel jLabel , String status){
        JPanel j = new JPanel();
        j.setLayout(new BorderLayout());
        j.add(jRadioButton,BorderLayout.WEST);
        j.add(jLabel,BorderLayout.CENTER);
        j.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if(e.getClickCount() == 2){
                    switch (status){
                        case "name":
                            sortName(Task.tasks);
                            sortName(Task.queueTasks);
                            sortName(Task.completedTasks);
                            DownloadManagerFrame.getInstance().doCompleted();
                            break;
                        case "size" :
                            sortSize(Task.tasks);
                            sortSize(Task.queueTasks);
                            sortSize(Task.completedTasks);
                            DownloadManagerFrame.getInstance().doCompleted();
                            break;
                        case "time" :
                            sortTime(Task.tasks);
                            sortTime(Task.queueTasks);
                            sortTime(Task.completedTasks);
                            DownloadManagerFrame.getInstance().doCompleted();
                            break;
                    }
                }
            }
        });
        return j;
    }

    /**
     * sorts the list by the size
     * @param unsortedList the list
     */
    private void sortTime(ArrayList<Task> unsortedList) {

        int size = unsortedList.size();
        int sortedSize=0;
        for(int i=1;i<size-1;i++){
            int j=i;
            do{
                if(unsortedList.get(j).timeCompareTo(unsortedList.get(j-1)) == -1){
                    Task small = unsortedList.get(j);
                    Task large = unsortedList.get(j-1);
                    unsortedList.set(j-1, small);
                    unsortedList.set(j, large);
                }
                j--;
            }while(j>sortedSize);

        }
    }

    /**
     * sorts the list by the size
     * @param unsortedList the list
     */
    private void sortName(ArrayList<Task> unsortedList) {

        int size = unsortedList.size();
        int sortedSize=0;
        for(int i=1;i<size-1;i++){
            int j=i;
            do{
                if(unsortedList.get(j).nameCompareTo(unsortedList.get(j-1)) == -1){
                    Task small = unsortedList.get(j);
                    Task large = unsortedList.get(j-1);
                    unsortedList.set(j-1, small);
                    unsortedList.set(j, large);
                }
                j--;
            }while(j>sortedSize);

        }
    }

    /**
     * sorts the list by the size
     * @param unsortedList the list
     */
    private void sortSize(ArrayList<Task> unsortedList) {

        int size = unsortedList.size();
        int sortedSize=0;
        for(int i=1;i<size-1;i++){
            int j=i;
            do{
                if(unsortedList.get(j).sizeCompareTo(unsortedList.get(j-1)) == -1){
                    Task small = unsortedList.get(j);
                    Task large = unsortedList.get(j-1);
                    unsortedList.set(j-1, small);
                    unsortedList.set(j, large);
                }
                j--;
            }while(j>sortedSize);

        }
    }




}
