package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * the setting frame
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class Setting extends JFrame implements ActionListener{

    //the limitation number
    private static Integer num;
    //the look and feel class name
    private static String LFClassName;
    //the downloading file location to be saved
    private static String location;
    //opens file chooser
    private JButton place;
    //oks info
    private JButton ok;
    //opens filter
    private JButton filterButton;
    //sort
    private JButton sortButton;
    //cancels the setting
    private JButton cancel;
    //the limitation number
    private JTextField jTextFieldNumber;
    //the default path to be downloaded in
    private JTextField jTextFieldPath;
    //a combo box containing all look and feels
    private JComboBox LandFs;
    //the file chooser
    private FileChooser fileChooser;
    //keeps the main frame in here (for updating it)
    private DownloadManagerFrame mainFrame;

    /**
     * constructor almost everything happens here
     */
    public Setting(DownloadManagerFrame mainFrame){
        super("Settings");
        this.mainFrame = mainFrame;
        this.setSize(400,200);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setResizable(false);
        this.setLayout(new GridLayout(4,1,5,5));
        BufferedImage icon = null;
        try {
            icon = ImageIO.read(new File("C:\\Users\\ASUS\\Downloads\\AP\\icon\\imageIcon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setIconImage(icon);
        makeFrame();
        this.setVisible(true);
    }

    /**
     * makes the frame
     */
    private void makeFrame(){
        jTextFieldPath = new JTextField();
        if(location == null || location.equals(""))
            location =  "C:\\Users\\ASUS\\Desktop";
        jTextFieldPath.setText(location);
        jTextFieldPath.setEnabled(false);
        filterButton = new JButton("Filters");
        filterButton.addActionListener(this);
        sortButton = new JButton("Sort");
        sortButton.addActionListener(this);
        ok = new JButton("OK");
        ok.addActionListener(this);
        cancel = new JButton("Cancel");
        cancel.addActionListener(this);
        place = new JButton(" Download Location");
        place.addActionListener(this);

        JLabel numLabel = new JLabel(" Max Simultaneous Downloads");
        JLabel jLabelLandF = new JLabel(" Look And Feel");
        jTextFieldNumber = new JTextField();
        if(num == null)
            jTextFieldNumber.setText("");
        else
            jTextFieldNumber.setText(num + "");

        if(LFClassName == null || LFClassName.equals(""))
            LFClassName = UIManager.getSystemLookAndFeelClassName();
        LandFs = new JComboBox();
        LandFs.addItem("Nimbus");
        LandFs.addItem("Windows");
        LandFs.addItem("Classic Windows");
        LandFs.addItem("Metal");
        LandFs.addItem("Motif");

        int ind = 0;
        switch (LFClassName){
            case "javax.swing.plaf.nimbus.NimbusLookAndFeel" :
                ind = 0;
                break;
            case "com.sun.java.swing.plaf.windows.WindowsLookAndFeel" :
                ind = 1;
                break;
            case "com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel" :
                ind = 2;
                break;
            case "javax.swing.plaf.metal.MetalLookAndFeel" :
                ind = 3;
                break;
            case "com.sun.java.swing.plaf.motif.MotifLookAndFeel" :
                ind = 4;
        }

        LandFs.setSelectedIndex(ind);
        JPanel pathPanel = new JPanel();
        pathPanel.setLayout(new GridLayout());
        pathPanel.add(jTextFieldPath);
        pathPanel.add(place);

        JPanel LFPanel = new JPanel();
        LFPanel.setLayout(new GridLayout());
        LFPanel.add(jLabelLandF);
        LFPanel.add(LandFs);

        JPanel numPanel = new JPanel();
        numPanel.setLayout(new GridLayout());
        numPanel.add(numLabel);
        numPanel.add(jTextFieldNumber);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.add(sortButton);
        buttonsPanel.add(filterButton);
        buttonsPanel.add(cancel);
        buttonsPanel.add(ok);

        this.add(pathPanel);
        this.add(LFPanel);
        this.add(numPanel);
        this.add(buttonsPanel);
    }

    /**
     * handles action listeners
     * @param e the Action event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if(o.equals(place)){
            fileChooser = new FileChooser(jTextFieldPath);
            this.repaint();
        }else if(o.equals(ok)){

            while(true) {
                try {
                    num = Integer.parseInt(jTextFieldNumber.getText());
                } catch (Exception ex) {
                    if(!jTextFieldNumber.getText().equals("")) {
                        JOptionPane.showMessageDialog(this, "Enter a number !");
                        break;
                    }
                }
                if(jTextFieldNumber.getText().equals(""))
                    num = null;

                switch (LandFs.getSelectedIndex()) {
                    case 0:
                        LFClassName = "javax.swing.plaf.nimbus.NimbusLookAndFeel";
                        break;
                    case 1:
                        LFClassName = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
                        break;
                    case 2:
                        LFClassName = "com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel";
                        break;
                    case 3:
                        LFClassName = "javax.swing.plaf.metal.MetalLookAndFeel";
                        break;
                    case 4:
                        LFClassName = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
                }

                try {
                    UIManager.setLookAndFeel(LFClassName);
                    SwingUtilities.updateComponentTreeUI(mainFrame);
                    mainFrame.getJToolBar().setLayout(new GridLayout(1, 6, 50, 5));
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                } catch (InstantiationException e1) {
                    e1.printStackTrace();
                } catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                } catch (UnsupportedLookAndFeelException e1) {
                    e1.printStackTrace();
                }
                if(fileChooser!=null)
                    location = fileChooser.getPath();
                else
                    location = jTextFieldPath.getText();
                this.dispose();
                break;
            }
        }else if(o.equals(cancel)){
            jTextFieldPath.setText("");
            jTextFieldNumber.setText("");
            LandFs.setSelectedIndex(0);
            this.dispose();
        }else if(o.equals(filterButton)){
            Filter filterFrame = new Filter();
        }else if(o.equals(sortButton)){
            Sort sort = new Sort();
        }
    }

    /**
     * accesses the location of file
     * @return location of downloading files
     */
    public static String getTheLocation() {
        return location;
    }

    /**
     * accesses the limitation number
     * @return the limitation number
     */
    public static Integer getNum() {
        return num;
    }

    /**
     * accesses the current look and feel
     * @return look and feel class name
     */
    public static String getLFClassName() {
        if(LFClassName == null || LFClassName.equals(""))
            LFClassName = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
        return LFClassName;
    }

    /**
     * sets num
     * @param num limit
     */
    public static void setNum(Integer num) {
        Setting.num = num;
    }

    /**
     * sets look and feel
     * @param LFClassName the look and feel
     */
    public static void setLFClassName(String LFClassName) {
        Setting.LFClassName = LFClassName;
    }

    /**
     * sets location
     * @param location location
     */
    public static void setLocation(String location) {
        Setting.location = location;
    }
}
