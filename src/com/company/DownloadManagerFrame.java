package com.company;


import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * the main frame
 * Singleton pattern
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class DownloadManagerFrame extends JFrame implements ActionListener {
    //menu bar
    private JMenuBar jMenuBar;
    //tool bar
    private JToolBar jToolBar;
    //toolbar button
    private JButton newDownload;
    //toolbar button
    private JButton pause;
    //toolbar button
    private JButton resume;
    //toolbar button
    private JButton cancel;
    //toolbar button
    private JButton remove;
    //toolbar button
    private JButton settings;
    //download menu
    private JMenu download;
    //new download item
    private JMenuItem newDownload1;
    //pause item
    private JMenuItem pause1;
    //resume item
    private JMenuItem resume1;
    //cancel item
    private JMenuItem cancel1;
    //remove item
    private JMenuItem remove1;
    //setting item
    private JMenuItem setting1;
    //exit item
    private JMenuItem exit;
    //the tray Icon
    private TrayIcon trayIcon;
    //the system tray
    private SystemTray tray;
    //the west panel
    private JPanel westPanel;
    //for processing downloads
    private JButton processing;
    //for completed downloads
    private JButton completed;
    //for waiting downloads
    private JButton queues;
    //search button
    private JButton search;
    //the status that shows wich panel is the contentPane
    private String status;
    //this frame
    private MouseListener mouseListener;
    //this frame
    private static DownloadManagerFrame jFrame;
    //the panel to be added in the center of the main panel for processing
    private static JPanel contentPane = new JPanel();
    //the panel to be added in the center of the main panel for in queue
    private static JPanel queueContentPane = new JPanel();
    //the panel to be added in the center of the main panel for completed
    private static JPanel completedContentPane = new JPanel();
    //all filtered URLs
    private static ArrayList<String> filters = new ArrayList<>();
    //the thread pool of processing
    private ExecutorService pool;
    //the thread pool of queue
    private ExecutorService QueuePool;


    /**
     * constructor almost everything happens here
     */
    private DownloadManagerFrame() {

        super("Java Download Manager");

        this.setContentPane(new ColorfulPanel(new Color(173, 206, 216), new Color(255, 255, 255)));
        this.setLayout(null);
        this.setResizable(false);
        this.setSize(1020, 750);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);

        jMenuBar = new JMenuBar();
        this.setJMenuBar(jMenuBar);
        makeHelpMenu();
        makeDownloadMenu();
        makeWestPanel();
        makeToolbarItems();

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        BufferedImage icon = null;
        try {
            icon = ImageIO.read(new File("C:\\Users\\ASUS\\Downloads\\AP\\icon\\imageIcon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setIconImage(icon);

        if (!SystemTray.isSupported())
            this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        tray = SystemTray.getSystemTray();
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File("C:\\Users\\ASUS\\Downloads\\AP\\icon\\imageIcon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        trayIcon = new TrayIcon(img, "TRAY");
        trayIcon.addActionListener(this);
        Handler h = new Handler(tray, trayIcon);//can add tooltip
        this.addWindowStateListener(h);
        this.addWindowListener(h);

        mouseListener = new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                if(e.getClickCount() == 2) {
                    open(Task.tasks,e);
                    open(Task.queueTasks,e);
                    open(Task.completedTasks,e);
                }else if(e.getModifiers() == MouseEvent.BUTTON3_MASK) {
                    appearInfo(Task.tasks,e);
                    appearInfo(Task.queueTasks,e);
                    appearInfo(Task.completedTasks,e);
                }

            }

            private void appearInfo(ArrayList<Task> tasks ,MouseEvent e){
                for(int i=0;i<tasks.size();i++)
                    if(e.getComponent().equals(tasks.get(i))) {
                        System.out.println("Task.size : " + tasks.size() + " index : " + i);
                        tasks.get(i).getInformation().appear();
                        break;
                    }
            }

            private void open(ArrayList<Task> tasks ,MouseEvent e) {
                for (Task task : tasks)
                    if (e.getComponent().equals(task)) {
                        System.out.println("NOtice me !!! : " + task.getName());
                        Desktop d = Desktop.getDesktop();
                        File f = new File(task.getFinalPath());
                        try {
                            d.open(f);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        break;
                    }
            }

        };
        this.setVisible(true);

    }

    /**
     * makes toolbar
     */
    private void makeToolbarItems(){
        jToolBar = new JToolBar();
        this.add(jToolBar);

        jToolBar.setBorderPainted(true);
        jToolBar.setLayout(new GridLayout(1, 6, 50, 5));
        jToolBar.setBounds(175, 0, 825, 40);
        jToolBar.setFloatable(false);
        jToolBar.setOpaque(false);

        newDownload = new JButton("", new ImageIcon("C:\\Users\\ASUS\\Downloads\\AP\\icon\\newDownload.jpg"));
        newDownload.setToolTipText("new download");
        makeToolbarButton(newDownload);

        pause = new JButton("", new ImageIcon("C:\\Users\\ASUS\\Downloads\\AP\\icon\\pause.jpg"));
        pause.setToolTipText("pause");
        makeToolbarButton(pause);

        resume = new JButton("", new ImageIcon("C:\\Users\\ASUS\\Downloads\\AP\\icon\\resume.jpg"));
        resume.setToolTipText("resume");
        makeToolbarButton(resume);

        cancel = new JButton("", new ImageIcon("C:\\Users\\ASUS\\Downloads\\AP\\icon\\cancel.jpg"));
        cancel.setToolTipText("cancel");
        makeToolbarButton(cancel);

        remove = new JButton("", new ImageIcon("C:\\Users\\ASUS\\Downloads\\AP\\icon\\remove.jpg"));
        remove.setToolTipText("remove");
        makeToolbarButton(remove);

        settings = new JButton("", new ImageIcon("C:\\Users\\ASUS\\Downloads\\AP\\icon\\setting.jpg"));
        settings.setToolTipText("settings");
        makeToolbarButton(settings);
    }

    /**
     * implements toolbar buttons features
     * @param jButton the toolbar button
     */
    private void makeToolbarButton(JButton jButton){
        jButton.setContentAreaFilled(false);
        jButton.addActionListener(this);
        jToolBar.add(jButton);
    }

    /**
     * make download menu
     */
    private void makeDownloadMenu(){
        download = new JMenu("Download");
        jMenuBar.add(download);
        download.setMnemonic('d');

        newDownload1 = new JMenuItem("New Download");
        download.add(newDownload1);
        newDownload1.setMnemonic('n');
        newDownload1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.ALT_DOWN_MASK));
        newDownload1.addActionListener(this);

        pause1 = new JMenuItem("Pause");
        download.add(pause1);
        pause1.setMnemonic('p');
        pause1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.ALT_DOWN_MASK));
        pause1.addActionListener(this);

        resume1 = new JMenuItem("Resume");
        download.add(resume1);
        resume1.setMnemonic('r');
        resume1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.ALT_DOWN_MASK));

        cancel1 = new JMenuItem("Cancel");
        download.add(cancel1);
        cancel1.setMnemonic('c');
        cancel1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_DOWN_MASK));
        cancel1.addActionListener(this);

        remove1 = new JMenuItem("Remove");
        download.add(remove1);
        remove1.setMnemonic('m');
        remove1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.ALT_DOWN_MASK));
        remove1.addActionListener(this);

        setting1 = new JMenuItem("Setting");
        download.add(setting1);
        setting1.setMnemonic('s');
        setting1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.ALT_DOWN_MASK));
        setting1.addActionListener(this);

        exit = new JMenuItem("Exit");
        exit.addActionListener(this);
        download.add(exit);
        exit.setMnemonic('e');
        exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.ALT_DOWN_MASK));
    }

    /**
     * makes help manue
     */
    private void makeHelpMenu(){
        JMenu help = new JMenu("Help");
        jMenuBar.add(help);
        help.setMnemonic('h');

        JMenuItem about = new JMenuItem("About");
        about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                About about1 = new About();
            }
        });
        help.add(about);
        about.setMnemonic('a');
        about.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.ALT_DOWN_MASK));
    }

    /**
     * makes west panel
     */
    private void makeWestPanel(){
        westPanel = new JPanel();
        westPanel.setLayout(null);
        westPanel.setBackground(new Color(6,61,88));
        westPanel.setBounds(0,0,170,690);
        this.add(westPanel);

        JButton jLabel = new JButton(new ImageIcon("C:\\Users\\ASUS\\Downloads\\AP\\icon\\cloud.jpg"));
        jLabel.setBounds(0,0,170,170);
        jLabel.setBackground(new Color(6,61,88));
        jLabel.setContentAreaFilled(false);
        westPanel.add(jLabel);

        processing = new JButton("Processing");
        processing.setBounds(0,170,170,40);
        makeWestPanelButton(processing);

        completed = new JButton("Completed ");
        completed.setBounds(0,210,170,40);
        makeWestPanelButton(completed);

        search = new JButton("  search  ");
        search.setBounds(0,290,170,40);
        makeWestPanelButton(search);

        queues = new JButton("  Queues  ");
        queues.setBounds(0,250,170,40);
        makeWestPanelButton(queues);
    }

    /**
     * makes west panel buttons
     * @param jButton
     */
    private void makeWestPanelButton(JButton jButton){
        jButton.setForeground(new Color(255,255,255));
        jButton.setBackground(new Color(6,61,88));
        jButton.addActionListener(this);
        westPanel.add(jButton);
    }

    /**
     * handles events
     * @param e Action command
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        Object o = e.getSource();
        if(o.equals(newDownload) || o.equals(newDownload1)) {
            Task task = new Task();
            NewDownloadFrame newDownloadFrame = new NewDownloadFrame();
            newDownloadFrame.getCancelButton().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    newDownloadFrame.dispose();
                }
            });

            newDownloadFrame.getOKButton().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isURLValid(newDownloadFrame.getURl().getText())) {
                        task.addTasks();
                        task.setInformation(new Information(newDownloadFrame.getURl(), newDownloadFrame.getFilePlaceTextField(), task.getTime()));
                        DownloadTask downloadTask = new DownloadTask(task, newDownloadFrame.getURl().getText(), newDownloadFrame.getFilePlaceTextField().getText());
                        Thread thread = new Thread(downloadTask);
                        //task.setDownloadTask(downloadTask);

                        if (newDownloadFrame.getQueueRadioButton().isSelected()) {
                            if (QueuePool == null) {
                                System.out.println(Setting.getNum() + "  is limit number apparently !!!!");
                                QueuePool = Executors.newFixedThreadPool(1);
                            }
                            QueuePool.execute(thread);
                            Task.queueTasks.add(task);
                            Task.queueTasks.get(Task.queueTasks.size() - 1).addMouseListener(mouseListener);
                            jFrame.add(makeContentPane(Task.queueTasks, queueContentPane));
                            doQueue();
                        } else {
                            if (pool == null) {
                                System.out.println(Setting.getNum() + "  is limit number apparently !!!!");
                                pool = Executors.newFixedThreadPool(Setting.getNum());
                            }
                            pool.execute(thread);
                            Task.tasks.add(task);
                            Task.tasks.get(Task.tasks.size() - 1).addMouseListener(mouseListener);
                            jFrame.add(makeContentPane(Task.tasks, contentPane));
                            doProcessing();
                        }

                        jFrame.setVisible(false);
                        jFrame.setVisible(true);
                        newDownloadFrame.dispose();
                    }else{
                        JOptionPane.showMessageDialog(newDownloadFrame,"Enter a valid url !");
                    }
                }
            });

            newDownloadFrame.requestFocus();
        }else if(o.equals(pause) || o.equals(pause1)){
            if(isSelected(Task.tasks))
                pauseSelected(Task.tasks);
            else if(isSelected(Task.queueTasks))
                pauseSelected(Task.queueTasks);
            else if(isSelected(Task.completedTasks))
                pauseSelected(Task.completedTasks);
        }else if(o.equals(resume) || o.equals(resume1)){
            if(isSelected(Task.tasks))
                resumeSelected(Task.tasks);
            else if(isSelected(Task.queueTasks))
                resumeSelected(Task.queueTasks);
            else if(isSelected(Task.completedTasks))
                resumeSelected(Task.completedTasks);
        }else if(o.equals(cancel) || o.equals(cancel1)){
            if(isSelected(Task.tasks))
                cancelSelected(Task.tasks);
            else if(isSelected(Task.queueTasks))
                cancelSelected(Task.queueTasks);
            else if(isSelected(Task.completedTasks))
                cancelSelected(Task.completedTasks);
        }else if(o.equals(remove) || o.equals(remove1)){

            if(isSelected(Task.tasks))
                removeSelected(Task.tasks,contentPane);
            else if(isSelected(Task.queueTasks))
                removeSelected(Task.queueTasks,queueContentPane);
            else if(isSelected(Task.completedTasks))
                removeSelected(Task.completedTasks,completedContentPane);

            SwingUtilities.updateComponentTreeUI(this);
            jToolBar.setLayout(new GridLayout(1, 6, 50, 5));

        }else if(o.equals(settings) || o.equals(setting1)){
            Setting setting = new Setting(this);
        }else if(o.equals(trayIcon)){
            this.setVisible(true);
            tray.remove(trayIcon);
        }else if(o.equals(exit)) {
            if(pool != null)
            pool.shutdown();
            Save save = new Save();
            System.exit(0);
        }else if(o.equals(processing)){
            doProcessing();
        }else if(o.equals(completed)){
            doCompleted();
        }else if(o.equals(queues)) {
            doQueue();
        }else if(o.equals(search)){
            Search searchWindow = new Search();
            //doSearch(searchWindow.getFound());
        }
    }

    /**
     * handles tray icon
     */
    private class Handler extends WindowAdapter {
        private final SystemTray tray;
        private final TrayIcon icon;

        public Handler(SystemTray tray, TrayIcon icon) {
            super();
            this.tray = tray;
            this.icon = icon;
        }

        private void addTrayIconDisposeFrame(JFrame frame) {
            try {
                tray.remove(icon);
                tray.add(icon);
                frame.setVisible(false);
            } catch (AWTException ex) {
                ex.printStackTrace();
            }
        }
        @Override public void windowStateChanged(WindowEvent e) {
            if (e.getNewState() == Frame.ICONIFIED) {
                addTrayIconDisposeFrame((JFrame) e.getSource());
            }
        }
        @Override public void windowClosing(WindowEvent e) {
            addTrayIconDisposeFrame((JFrame) e.getSource());
        }

    }

    /**
     * accesses toolbar
     * @return toolbar
     */
    public JToolBar getJToolBar() {
        return jToolBar;
    }

    /**
     *
     * @param tasks all downloads we wanna appear in the frame
     * @return a jPanel consisting tasks that'll be added to main frame
     */
    private JPanel makeContentPane(ArrayList<Task> tasks,JPanel contentPane){
        contentPane.setLayout(new GridLayout(10,1,5,5));
        contentPane.setBounds(170,60,831,619);
        for (Task task : tasks) contentPane.add(task);
        contentPane.setOpaque(false);
        return contentPane;
    }

    /**
     * removes selected tasks from the jPanel
     * @param tasks removes from this array list of tasks
     * @param jPanel the panel we wanna remove from
     */
    private void removeSelected(ArrayList<Task> tasks,JPanel jPanel){
        for (int i = 0; i < tasks.size(); i++)
            if(tasks.get(i).getProgressPanelRadioButton().isSelected()){

                Information information = tasks.get(i).getInformation();
                try(FileWriter fileWriter = new FileWriter("removed.jdm" , true)) {
                    fileWriter.write("URL : " + information.getUrl().getText() + "\n");
                    fileWriter.write("Path : " + information.getPath().getText() + "\n");
                    fileWriter.write("Size : " + information.getSizeofFile()+ "\n-------\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }

                jPanel.remove(tasks.get(i));
                tasks.remove(i);
                i--;
            }
    }

    /**
     * cancels the selected Tasks
     * @param tasks the array list of Tasks
     */
    private void cancelSelected(ArrayList<Task> tasks){
        for (int i = 0; i < tasks.size(); i++)
            if(tasks.get(i).getProgressPanelRadioButton().isSelected()){
                tasks.get(i).cancel();
            }
    }

    /**
     * makes all the selected tasks not selected
     * @param tasks array list of tasks
     */
    private void makeNotSelected(ArrayList<Task> tasks){
        for(Task t : tasks)
            t.getProgressPanelRadioButton().setSelected(false);
    }

    /**
     * pauses the selected Tasks
     * @param tasks the array list of Tasks
     */
    private void pauseSelected(ArrayList<Task> tasks){
        for (Task task : tasks)
            if (task.getProgressPanelRadioButton().isSelected())
                task.pause();
    }

    /**
     * resumes selected Tasks
     * @param tasks the array list of Tasks
     */
    private void resumeSelected(ArrayList<Task> tasks){
        for (Task task : tasks) {
            if (task.getProgressPanelRadioButton().isSelected()) {
                task.resume();
            }
        }
    }

    /**
     * is called when processing button is clicked
     * shows processing panel
     */
    private void doProcessing(){
        status = "normal";
        makeBlack();
        processing.setForeground(Color.RED);
        search.setForeground(new Color(173, 206, 216));
        completed.setForeground(new Color(173, 206, 216));
        queues.setForeground(new Color(173, 206, 216));
        makeNotSelected(Task.completedTasks);
        makeNotSelected(Task.queueTasks);
        this.remove(queueContentPane);
        this.remove(completedContentPane);
        jFrame.add(makeContentPane(Task.tasks,contentPane));
        SwingUtilities.updateComponentTreeUI(this);
        jToolBar.setLayout(new GridLayout(1, 6, 50, 5));
        colorButtons();
    }

    /**
     * is called when completed button is clicked
     * shows completed panel
     */
    public void doCompleted(){
        status = "completed";
        makeBlack();
        completed.setForeground(Color.RED);
        search.setForeground(new Color(173, 206, 216));
        processing.setForeground(new Color(173, 206, 216));
        queues.setForeground(new Color(173, 206, 216));
        makeNotSelected(Task.queueTasks);
        makeNotSelected(Task.tasks);
        this.remove(queueContentPane);
        this.remove(contentPane);
        jFrame.add(makeContentPane(Task.completedTasks,completedContentPane));
        SwingUtilities.updateComponentTreeUI(this);
        jToolBar.setLayout(new GridLayout(1, 6, 50, 5));
        colorButtons();
    }

    /**
     * is called when queue button is clicked
     * shows queue panel
     */
    private void doQueue(){
        status = "queue";
        makeBlack();
        queues.setForeground(Color.RED);
        completed.setForeground(new Color(173, 206, 216));
        processing.setForeground(new Color(173, 206, 216));
        search.setForeground(new Color(173, 206, 216));
        makeNotSelected(Task.completedTasks);
        makeNotSelected(Task.tasks);
        this.remove(completedContentPane);
        this.remove(contentPane);
        jFrame.add(makeContentPane(Task.queueTasks,queueContentPane));
        SwingUtilities.updateComponentTreeUI(this);
        jToolBar.setLayout(new GridLayout(1, 6, 50, 5));
        colorButtons();
    }

//    private void doSearch(ArrayList<Task> searchTasks){
//        search.setForeground(Color.RED);
//        completed.setForeground(new Color(173, 206, 216));
//        processing.setForeground(new Color(173, 206, 216));
//        queues.setForeground(new Color(173, 206, 216));
//        makeNotSelected(Task.completedTasks);
//        makeNotSelected(Task.tasks);
//        makeNotSelected(Task.queueTasks);
//        this.remove(completedContentPane);
//        this.remove(contentPane);
//        this.remove(queueContentPane);
//        jFrame.add(makeContentPane(searchTasks,queueContentPane));
//        SwingUtilities.updateComponentTreeUI(this);
//        jToolBar.setLayout(new GridLayout(1, 6, 50, 5));
//        colorButtons();
//    }

    /**
     * founds out which one of the panes are selected
     * @param tasks array list of Tasks
     * @return true if the pane is selected and false otherwise
     */
    private boolean isSelected(ArrayList<Task> tasks){
        for(Task t : tasks){
            if(t.getProgressPanelRadioButton().isSelected())
                return true;
        }
        return false;
    }

    /**
     * makes an instance of this class if jFrame is null
     * @return the instance of this class
     */
    public static DownloadManagerFrame getInstance(){
        if(jFrame == null)
            jFrame = new DownloadManagerFrame();
        return jFrame;
    }

    /**
     * accesses the mouse listener
     * @return the mouse listener
     */
    public MouseListener getMouseListener() {
        return mouseListener;
    }

    /**
     * accesses filters
     * @return filters
     */
    public static ArrayList<String> getFilters() {
        return filters;
    }

    /**
     * saves all the string to the filters
     * @param strings the array of strings
     */
    public static void saveFilters(String[] strings){
        filters.addAll(Arrays.asList(strings));
    }

    /**
     * checks if the url is valid
     * @param s the url
     * @return true if it is valid and false otherwise
     */
    private boolean isURLValid(String s){
        try {
            URL url = new URL(s);
        } catch (MalformedURLException e) {
            return false;
        }
        for(String string : filters){
            if(string.contains(s)) {
                System.out.println(s);
                System.out.println(string);
                return false;
            }
        }
        return true;
    }

    /**
     * color every button in west panel
     */
    public void colorButtons(){
        processing.setBackground(new Color(6,61,88));
        processing.repaint();
        queues.setBackground(new Color(6,61,88));
        queues.repaint();
        search.setBackground(new Color(6,61,88));
        search.repaint();
        completed.setBackground(new Color(6,61,88));
        completed.repaint();
        SwingUtilities.updateComponentTreeUI(this);
    }

    /**
     * makes all Task name label black
     */
    private void makeBlack(){
        for(Task t: Task.tasks)
            t.getLabelName().setForeground(Color.BLACK);
        for(Task t: Task.completedTasks)
            t.getLabelName().setForeground(Color.BLACK);
        for(Task t: Task.queueTasks)
            t.getLabelName().setForeground(Color.BLACK);
    }

    /**
     * accesses the status
     * @return
     */
    public String getStatus() {
        return status;
    }
}