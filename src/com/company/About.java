package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * the "about" frame shows info about program
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class About extends JFrame{

    /**
     * constructor (everything happens here)
     */
    public About(){
        JLabel jLabel = new JLabel("  Student ID : 9631077");
        JLabel jLabel1 = new JLabel("  Name : Ghazale Noroozi");
        JLabel jLabel2 = new JLabel("  Start Day : 1397,2,22");
        JLabel jLabel3 = new JLabel("  End Day : 1397,2,29");

        this.setContentPane(new ColorfulPanel(new Color(173, 206, 216), new Color(255, 255, 255)));
        this.setLayout(new GridLayout(5,1,1,1));
        this.add(jLabel);
        this.add(jLabel1);
        this.add(jLabel2);
        this.add(jLabel3);

        this.setSize(500,200);
        this.setResizable(false);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        BufferedImage icon = null;
        try {
            icon = ImageIO.read(new File("C:\\Users\\ASUS\\Downloads\\AP\\icon\\imageIcon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.setIconImage(icon);
        this.setVisible(true);
    }
}
