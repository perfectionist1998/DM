package com.company;

import java.awt.*;
import javax.swing.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;

/**
 * handles the new download button totally. (including opening a new download frame and a panel in main frame
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class Task extends JPanel implements Serializable{
    //all processing files
    public static ArrayList<Task> tasks = new ArrayList<>();
    //all in queue files
    public static ArrayList<Task> queueTasks = new ArrayList<>();
    //all completed files
    public static ArrayList<Task> completedTasks = new ArrayList<>();
    //the J label that shows total size of the file
    private JLabel jLabelSize;
    //the J label that shows downloaded size of the file
    private JLabel jLabelSizeDownloaded;
    //the J label that shows download percent
    private JLabel jLabelPercentDownloaded;
    // the J label that shows download speed
    private JLabel jLabelSpeed;
    //the J label that shows the downloading file final path and name in this PC
    private JLabel labelName;
    //download speed
    private int speed;
    //progress panel radio button
    private JRadioButton progressPanelRadioButton;
    //the Task's information
    private Information information;
    //downloading file name
    private String name;
    //downloading file size
    private int size;
    //downloading file size downloaded
    private int sizeDownloaded;
    //time download begins
    private Time time;
    //the state of download
    private int isPaused;
    //the progress bar
    private JProgressBar jProgressBar;
    private String finalPath;


    /**
     * constructor almost everything happens here
     * @param name downloading file name
     * @param size downloading file size
     */
    public Task(String name,int size){
        super();
        speed = 0;
        information = null;
        sizeDownloaded = 0;
        this.name = name;
        this.size = size;
    }

    /**
     * constructor with no parameter
     */
    public Task(){
        this("",0);
    }

    /**
     * makes a new task panel and returns them in a JPanel
     * @param time downloading start time
     */
    private void addTasks(Time time){
        this.time = time;
        this.setOpaque(false);
        this.setSize(300,50);
        this.setLayout(new BorderLayout());

        jProgressBar = new JProgressBar(SwingConstants.HORIZONTAL, 0, 100);
        jProgressBar.setBorderPainted(false);
        jProgressBar.setBounds(0, 0, 500, 500);
        jProgressBar.setValue(0);
        jProgressBar.setStringPainted(true);
        this.add(jProgressBar,BorderLayout.CENTER);

        labelName = new JLabel(name + "  name");
        this.add(labelName , BorderLayout.NORTH);

        progressPanelRadioButton = new JRadioButton();
        this.add(progressPanelRadioButton,BorderLayout.WEST);

        jLabelSize = new JLabel("  " + size + " KB ");
        jLabelSizeDownloaded = new JLabel(sizeDownloaded + "downloaded ");
        jLabelPercentDownloaded = new JLabel(sizeDownloaded*100/(size+1) + "% downloaded ");
        jLabelSpeed = new JLabel(" speed : " + speed);

        JPanel jPanel = new JPanel(new GridLayout(1,4,2,2));
        jPanel.setOpaque(false);
        jPanel.add(jLabelSize);
        jPanel.add(jLabelSizeDownloaded);
        jPanel.add(jLabelPercentDownloaded);
        jPanel.add(jLabelSpeed);

        this.add(jPanel , BorderLayout.SOUTH);
    }

    /**
     * makes a new task panel and returns them in a JPanel
     */
    public void addTasks(){
        addTasks(new Time(System.currentTimeMillis()));
    }

    /**
     *accesses the task information frame
     * @return the information frame
     */
    public Information getInformation() {
        return information;
    }

    /**
     * accesses the progress panel radio button
     * @return
     */
    public JRadioButton getProgressPanelRadioButton() {
        return progressPanelRadioButton;
    }

    /**
     * accesses time
     * @return time
     */
    public Time getTime() {
        return time;
    }

    /**
     * sets the information
     * @param information information
     */
    public void setInformation(Information information) {
        this.information = information;
    }

    /**
     * sets the progressbar value
     * @param value value
     */
    public void setProgressBarValue(int value){
        jProgressBar.setValue(value);
    }

    /**
     * sets the label size
     * @param size the label size
     */
    public void setLabelSize(double size){
        jLabelSize.setText("  " + size + " KB ");
    }

    /**
     * sets the label size downloaded
     * @param sizeDownloaded the label size downloaded
     */
    public void setLabelSizeDownloaded(int sizeDownloaded){jLabelSizeDownloaded.setText(sizeDownloaded + " KB downloaded ");}

    /**
     * sets the percent of download
     * @param percentDownloaded percent of download
     */
    public void setLabelPercentDownloaded(int percentDownloaded){
        jLabelPercentDownloaded.setText(percentDownloaded + "% downloaded ");
    }

    /**
     * sets label name
     * @param name name
     */
    public void setLabelName(String name){
        labelName.setText(name);
    }

    /**
     * sets label speed
     * @param speed the speed
     */
    public void setLabelSpeed(double speed){
        jLabelSpeed.setText(" speed : " + speed + " KB/sec");
    }

    /**
     * pauses the task
     */
    public final void pause() {
        if (isPaused() == 0) {
            isPaused = 1;
            firePropertyChange("paused", false, true);
        }
    }

    /**
     * resumes the task
     */
    public final void resume() {
        if (isPaused() == 1) {
            isPaused = 0;
            firePropertyChange("paused", true, false);
        }
    }

    /**
     * cancels the task
     */
    public final void cancel(){
        pause();
        isPaused = -1;
    }

    /**
     * accesses the is paused
     * @return is paused
     */
    public int isPaused(){
        return isPaused;
    }

    /**
     * gets the label name string
     * @return the task name
     */
    @Override
    public String getName() {
        return labelName.getText();
    }

    /**
     * sets the final path
     * @param s the final path
     */
    public void setFinalPath(String s){
        finalPath = s;
    }

    /**
     * accesses the final path
     * @return the final path
     */
    public String getFinalPath(){
        return finalPath;
    }

    /**
     * accesses the name label
     * @return name label
     */
    public JLabel getLabelName() {
        return labelName;
    }

    /**
     * compares two tasks by time started
     * @param task the other tasks
     * @return 1 if this is bigger and -1 if task is bigger and 0 otherwise
     */
    public int timeCompareTo(Task task){
        if(task.getTime().getTime() > this.getTime().getTime())
            return -1;
        else if(task.getTime().getTime() == this.getTime().getTime())
            return 0;
        else
            return 1;
    }

    /**
     * compares two tasks by size
     * @param task the other task
     * @return 1 if this is bigger and -1 if task is bigger and 0 otherwise
     */
    public int sizeCompareTo(Task task){
        if(task.getInformation().getSizeofFile() > this.getInformation().getSizeofFile())
            return -1;
        else if(task.getInformation().getSizeofFile() == this.getInformation().getSizeofFile())
            return 0;
        else
            return 1;
    }

    /**
     * compares two tasks by name
     * @param task the other task
     * @return 1 if this is bigger and -1 if task is bigger and 0 otherwise
     */
    public int nameCompareTo(Task task){
        return this.getLabelName().getText().compareTo(task.getLabelName().getText());
    }
}
