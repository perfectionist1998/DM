package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Time;

/**
 *  shows info about each task
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class Information extends JFrame {

    private JLabel url;
    //private JTextField urlTextField;
    private JLabel path;
    //private JTextField pathTextField;
    private JLabel timeLabel;
    private double size;

    /**
     * constructor (everything happens here)
     */
    public Information(JTextField url, JTextField path , Time timeLabel){

        this.url = new JLabel(url.getText());
        //this.urlTextField = url;
        this.path = new JLabel(path.getText());
        //this.pathTextField = path;
        this.timeLabel = new JLabel(timeLabel.toString());

        this.setLayout(new GridLayout(5,1,1,1));
        this.add(this.url);
        this.add(this.path);
        this.add(this.timeLabel);

        this.setSize(500,200);
        this.setResizable(false);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        BufferedImage icon = null;
        try {
            icon = ImageIO.read(new File("C:\\Users\\ASUS\\Downloads\\AP\\icon\\imageIcon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setIconImage(icon);
    }

    /**
     * the file time
     * @param time the file time
     */
    public void setTime(JLabel time) {
        this.timeLabel = time;
    }

    /**
     * sets path
     * @param path teh file path
     */
    public void setPath(JLabel path) {
        this.path = path;
    }

    /**
     * sets url
     * @param url the file url
     */
    public void setUrl(JLabel url) {
        this.url = url;
    }

    /**
     * accesses the url j label
     * @return the url label
     */
    public JLabel getUrl() {
        return url;
    }

    /**
     * accesses the path label
     * @return path label
     */
    public JLabel getPath() {
        return path;
    }

    /**
     * accesses the file size
     * @return the file size
     */
    public double getSizeofFile() {
        return size;
    }

    /**
     * makes panel appear
     */
    public void appear(){
        this.setVisible(true);
    }
}
