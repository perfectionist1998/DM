package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * the search frame opens when search button is clicked
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class Search extends JFrame implements ActionListener {
    //the textField
    private JTextField jTextField;
    //OK button
    private JButton ok;

    /**
     * constructor that make frame and initializes fields
     */
    public Search(){
        this.setLayout(new BorderLayout());
        jTextField = new JTextField();
        ok = new JButton("OK");
        ok.addActionListener(this);
        this.add(jTextField,BorderLayout.CENTER);
        this.add(ok,BorderLayout.SOUTH);
        this.setSize(500,120);
        this.setResizable(true);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        BufferedImage icon = null;
        try {
            icon = ImageIO.read(new File("C:\\Users\\ASUS\\Downloads\\AP\\icon\\imageIcon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setIconImage(icon);
        this.setVisible(true);
    }

    /**
     * handles events
     * @param e Action command
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("OK"))
            if(!jTextField.getText().equals("") && jTextField.getText() != null) {
                if(DownloadManagerFrame.getInstance().getStatus().equals("normal"))
                    search(Task.tasks , jTextField.getText());
                if(DownloadManagerFrame.getInstance().getStatus().equals("completed"))
                    search(Task.completedTasks , jTextField.getText());
                if(DownloadManagerFrame.getInstance().getStatus().equals("queue"))
                    search(Task.queueTasks , jTextField.getText());
            this.dispose();
        }
    }

    /**
     * searches on an array list of Tasks.
     * @param tasks the array list of Tasks
     * @param s the string that is searched
     */
    private void search(ArrayList<Task> tasks , String s){
        for(Task t : tasks)
            if(t.getInformation().getUrl().getText().contains(s) || t.getName().contains(s)) {
                t.getLabelName().setForeground(Color.RED);
                t.repaint();
            }
    }
}
