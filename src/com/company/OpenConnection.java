package com.company;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * opens a connection and makes input stream
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class OpenConnection {
    private HttpURLConnection httpURLConnection;
    private String fileName;
    private InputStream inputStream;
    private int contentLength;

    /**
     * the constructor
     * @param urlString the url string
     * @throws Exception the exception happens
     */
    OpenConnection(String urlString) throws Exception {

        URL url = new URL(urlString);
        httpURLConnection = (HttpURLConnection) url.openConnection();
        int responseCode = httpURLConnection.getResponseCode();

        if(responseCode / 100 == 2){

            contentLength = httpURLConnection.getContentLength();
            fileName = urlString.substring(urlString.lastIndexOf("/") + 1,urlString.length());
            inputStream = httpURLConnection.getInputStream();

        }else{
            throw new IOException("No file to download. Server replied HTTP code: " + responseCode);
        }
    }

    /**
     * closes input stream and disconnects the connection
     * @throws IOException
     */
    public void disconnect() throws IOException {
        inputStream.close();
        httpURLConnection.disconnect();
    }

    /**
     * accesses the file name
     * @return the file name
     */
    public String getFileName() {
        return this.fileName;
    }

    /**
     * accesses the content length
     * @return the content length
     */
    public int getContentLength() {
        return this.contentLength;
    }

    /**
     * accesses the input stream
     * @return the input stream
     */
    public InputStream getInputStream() {
        return this.inputStream;
    }
}
