package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * opens when filter is clicked
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class Filter extends JFrame implements ActionListener {

    private JTextArea jTextArea;
    //oks info
    private JButton ok;
    //cancels the setting
    private JButton cancel;
    //edits the filters
    private JButton edit;
     //not used
    final JScrollPane scrolll = new JScrollPane();

    /**
     * constructor that initializes fields
     */
    public Filter(){
        //System.out.println("textArea initial :"+temp.toString());
        jTextArea = new JTextArea(Load.getInitialTextArea());
        jTextArea.setLineWrap(true);
        jTextArea.setBounds(0,0,500,200);
        scrolll.add(jTextArea);
        edit = new JButton("edit");
        edit.addActionListener(this);
        ok = new JButton("OK");
        ok.addActionListener(this);
        cancel = new JButton("Cancel");
        cancel.addActionListener(this);
        JPanel jPanel = new JPanel();
        jPanel.add(cancel);
        jPanel.add(ok);

        this.setLayout(new BorderLayout());
        this.add(jTextArea,BorderLayout.CENTER);
        //this.add(scrolll,BorderLayout.CENTER);
        this.add(jPanel,BorderLayout.SOUTH);

        this.setSize(500,120);
        this.setResizable(true);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        BufferedImage icon = null;
        try {
            icon = ImageIO.read(new File("C:\\Users\\ASUS\\Downloads\\AP\\icon\\imageIcon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setIconImage(icon);
        this.setVisible(true);
    }

    /**
     * handles events
     * @param e action event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if(o.equals(ok)){
            String s[] = jTextArea.getText().split("\\r?\\n");
           // ArrayList<String>arrList = new ArrayList<>(Arrays.asList(s)) ;
            boolean shouldClose = true;
            try( FileWriter fileWriter = new FileWriter("filter.jdm")) {

                for (int i = 0; i < s.length; i++) {
                    if(checkValidity(s[i])){
                        try{
                            fileWriter.write(s[i] + "\n");
                        } catch (MalformedURLException e1) {
                            e1.printStackTrace();
                        }
                    }else{
                        JOptionPane.showMessageDialog(this, "Enter a valid URL.");
                        shouldClose = false;
                        break;
                    }
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }

            if(shouldClose) {
                DownloadManagerFrame.saveFilters(s);
                this.dispose();
            }

        }else if(o.equals(cancel)){
            this.dispose();
        }else if(o.equals(edit)){

        }
    }

    /**
     * checks if the url is valid
     * @param s the url
     * @return true if it is valid and false otherwise
     */
    private boolean checkValidity(String s){
        try {
            URL url = new URL(s);
        } catch (MalformedURLException e) {
            return false;
        }
        return true;
    }
}
