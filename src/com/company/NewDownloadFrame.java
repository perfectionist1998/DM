package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * new download frame
 * @author Ghazale Noroozi
 * @version 0.0
 */
public class NewDownloadFrame extends JFrame implements ActionListener {
    //opens the file chooser in new download frame
    private JButton chooseFileButton;
    //cancels a new download
    private JButton cancelButton;
    //starts a new download
    private JButton OKButton;
    //downloading file url
    private JTextField URl;
    //downloading file where to be placed in PC
    private JTextField filePlaceTextField;
    //provides the ability to add to queue
    private JRadioButton queueRadioButton;

    public NewDownloadFrame(){
        this.setLayout(new BorderLayout());

        this.setSize(600,140);
        this.setResizable(false);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

        BufferedImage icon = null;
        try {
            icon = ImageIO.read(new File("C:\\Users\\ASUS\\Downloads\\AP\\icon\\imageIcon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setIconImage(icon);

        makeFrame();

        this.setVisible(true);
    }

    /**
     * makes the new download frame
     */
    private void makeFrame(){
        this.setLayout(new BorderLayout());
        //TODO replace with "URL"
        URl = new JTextField("https://as1.asset.aparat.com/aparat-video/8bdbe0381e831fb6a9e252966b801aad1945607__40797.mp4");
        this.add(URl,BorderLayout.NORTH);

        filePlaceTextField = new JTextField(Setting.getTheLocation());
        filePlaceTextField.setEnabled(false);
        chooseFileButton = new JButton("open");
        chooseFileButton.addActionListener(this);

        JPanel Panel = new JPanel(new BorderLayout());
        Panel.add(filePlaceTextField,BorderLayout.CENTER);
        Panel.add(chooseFileButton,BorderLayout.EAST);
        this.add(Panel,BorderLayout.CENTER);

        cancelButton = new JButton("Cancel");
        OKButton = new JButton("OK");
        queueRadioButton = new JRadioButton("Add to Queue");
        JPanel smallerJPanel = new JPanel();
        smallerJPanel.add(cancelButton);
        smallerJPanel.add(OKButton);
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout());
        jPanel.add(queueRadioButton,BorderLayout.WEST);
        jPanel.add(smallerJPanel,BorderLayout.EAST);
        this.add(jPanel,BorderLayout.SOUTH);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if(o.equals(chooseFileButton)){
            FileChooser fileChooser = new FileChooser(filePlaceTextField);
        }
    }

    /**
     * accesses the OK button
     * @return the OK button
     */
    public JButton getOKButton() {
        return OKButton;
    }

    /**
     * accesses the Cancel button
     * @return the Cancel button
     */
    public JButton getCancelButton() {
        return cancelButton;
    }

    /**
     * accesses queueRadioButton
     * @return the queueRadioButton
     */
    public JRadioButton getQueueRadioButton() {
        return queueRadioButton;
    }

    /**
     * accesses url text filed
     * @return the url text field
     */
    public JTextField getURl() {
        return URl;
    }

    /**
     * accesses the directory
     * @return the file directory
     */
    public JTextField getFilePlaceTextField() {
        return filePlaceTextField;
    }
}
